package com.tonne.ag04zadatak.Helpers;

public class Constants {

    public static String mainUrl = "https://api.themoviedb.org/3/";
    public static String apiKey = "?api_key=fe3b8cf16d78a0e23f0c509d8c37caad";

    public static String getTopRatedUrl(String page){
        return mainUrl+"movie/top_rated"+apiKey+"&page="+page;
    }

    public static String getPopularUrl(String page){
        return mainUrl+"movie/popular"+apiKey+"&page="+page;
    }

    public static String getSimilarMovies(String movieId){
        return mainUrl+"movie/"+movieId+"/similar"+apiKey;
    }

    public static String imageUrlPath="http://image.tmdb.org/t/p/w185";

    public static String getImageUrlPath(String imgId){
        return imageUrlPath+imgId;
    }

    public static String getMovieDetails(String movieId){
        return mainUrl+"movie/"+movieId+apiKey;
    }
}
