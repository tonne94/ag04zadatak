package com.tonne.ag04zadatak.Helpers;

import java.util.ArrayList;

public class MovieDetails {
    private String title;
    private String release_date;
    private ArrayList<String> genres;
    private String overview;
    private String poster_path;
    private String backdrop_path;

    public MovieDetails(String title, String release_date, ArrayList<String> genres, String overview, String poster_path, String backdrop_path){
        this.title = title;
        this.release_date = release_date;
        this.genres = genres;
        this.overview = overview;
        this.poster_path = poster_path;
        this.backdrop_path = backdrop_path;
    }

    public String getTitle() {
        return title;
    }

    public String getReleaseDate() {
        return release_date;
    }

    public ArrayList<String> getGenres() {
        return genres;
    }

    public String getOverview() {
        return overview;
    }

    public String getPosterPath() {
        return poster_path;
    }

    public String getBackdropPath() {
        return backdrop_path;
    }
}
