package com.tonne.ag04zadatak.Helpers;

public class Movie {
    private int id;
    private String title;
    private String poster_path;

    public Movie(int id, String title, String poster_path){
        this.id = id;
        this.title = title;
        this.poster_path = poster_path;
    }

    public int getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getPosterPath() {
        return this.poster_path;
    }
}
