package com.tonne.ag04zadatak;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tonne.ag04zadatak.Helpers.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MovieDetailsScreen extends AppCompatActivity {
    private static String TAG = "AG04ZADATAKTAG";
    private OkHttpClient client = new OkHttpClient();
    private String jsonResponseMovieDetails, jsonResponseSimilarMovies;

    ImageView imageViewPoster;
    ImageView imageViewBackdrop;
    TextView textViewTitle;
    TextView textViewReleaseDate;
    TextView textViewGenres;
    TextView textViewOverview;
    LinearLayout linearLayoutSimilarMovies;
    ProgressBar progressBar;
    ConstraintLayout constraintLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        imageViewPoster = findViewById(R.id.imageViewPoster);
        imageViewBackdrop = findViewById(R.id.imageViewBackdrop);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewReleaseDate = findViewById(R.id.textViewReleaseDate);
        textViewGenres = findViewById(R.id.textViewGenres);
        textViewOverview = findViewById(R.id.textViewOverview);
        linearLayoutSimilarMovies = findViewById(R.id.linearLayoutSimilarMovies);
        constraintLayout = findViewById(R.id.constraintLayout);
        progressBar = findViewById(R.id.progressBar);
        constraintLayout.setVisibility(View.GONE);
        Intent intent = getIntent();
        Integer movie_id = intent.getIntExtra("movie_id", 0);
        sendRequestMovieDetails(movie_id.toString());
        sendRequestSimilarMovies(movie_id.toString());
    }

    private void processJsonMovieDetails() throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonResponseMovieDetails);
        String poster_path = jsonObject.getString("poster_path");
        String backdrop_path = jsonObject.getString("backdrop_path");
        if(backdrop_path!="null" && backdrop_path != null){
            Picasso.get().load(Constants.getImageUrlPath(backdrop_path)).into(imageViewBackdrop, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    constraintLayout.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {

                }
            });
            Picasso.get().load(Constants.getImageUrlPath(poster_path)).into(imageViewPoster);
        }else{
            if(poster_path != "null" && poster_path != null){
                Picasso.get().load(Constants.getImageUrlPath(poster_path)).into(imageViewPoster, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        constraintLayout.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
            }else{
                constraintLayout.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        }

        String title = jsonObject.getString("title");
        String release_date = jsonObject.getString("release_date");
        String overview = jsonObject.getString("overview");
        JSONArray genresArray = jsonObject.getJSONArray("genres");
        String genres="";
        for(int i=0; i<genresArray.length(); i++){
            JSONObject genre = genresArray.getJSONObject(i);
            if(i!=0){
                genres+=" ";
            }
            genres+=genre.getString("name");
            if(i!=genresArray.length()-1){
                genres+=",";
            }
        }
        textViewTitle.setText(title);
        textViewReleaseDate.setText(release_date);
        textViewGenres.setText(genres);
        textViewOverview.setText(overview);
    }

    private void sendRequestMovieDetails(String movieId){
        constraintLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        String url = Constants.getMovieDetails(movieId);
        final Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Integer code = response.code();
                if(response.code()==200){
                    jsonResponseMovieDetails = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processJsonMovieDetails();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }

    private void processJsonSimilarMovies() throws JSONException {

        JSONObject jsonObject = new JSONObject(jsonResponseSimilarMovies);
        JSONArray results = jsonObject.getJSONArray("results");
        if(results.length()>0){
            linearLayoutSimilarMovies.removeAllViews();
            for(int i=0; i<5; i++){
                JSONObject movieObject = results.getJSONObject(i);
                TextView textViewTitle = new TextView(this);
                textViewTitle.setText(movieObject.getString("title"));
                textViewTitle.setTextColor(Color.BLACK);
                textViewTitle.setTextSize(18);
                textViewTitle.setPadding(16,8,8,8);
                textViewTitle.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
                ));
                final Integer movie_id = Integer.parseInt(movieObject.getString("id"));
                textViewTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), MovieDetailsScreen.class);
                        intent.putExtra("movie_id", movie_id);
                        startActivity(intent);
                    }
                });
                linearLayoutSimilarMovies.addView(textViewTitle);
            }

        }
    }

    private void sendRequestSimilarMovies(String movieId){
        String url = Constants.getSimilarMovies(movieId);
        final Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Integer code = response.code();
                if(response.code()==200){
                    jsonResponseSimilarMovies = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                processJsonSimilarMovies();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }
}
