package com.tonne.ag04zadatak;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.tonne.ag04zadatak.Helpers.Constants;
import com.tonne.ag04zadatak.Helpers.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

enum Mode {
    TOP_RATED,
    POPULAR
}

public class MainActivity extends AppCompatActivity {

    private static String TAG = "AG04ZADATAKTAG";
    private OkHttpClient client = new OkHttpClient();
    private String jsonResponse;
    private ArrayList<Movie> movies = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerViewMovies;
    private MoviesAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Integer page=1;
    private Handler handler = new Handler();
    private BottomNavigationView bottomNavigationView;
    private Mode mode = Mode.TOP_RATED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_top_rated:
                        mode = Mode.TOP_RATED;
                        movies.clear();
                        adapter.notifyDataSetChanged();
                        page = 1;
                        sendRequest(page.toString());
                        swipeRefreshLayout.setRefreshing(true);
                        return true;
                    case R.id.navigation_popular:
                        mode = Mode.POPULAR;
                        movies.clear();
                        adapter.notifyDataSetChanged();
                        page = 1;
                        sendRequest(page.toString());
                        swipeRefreshLayout.setRefreshing(true);
                        return true;
                }
                return false;
            }
        });
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                movies.clear();
                adapter.notifyDataSetChanged();
                page = 1;
                sendRequest(page.toString());
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        recyclerViewMovies = findViewById(R.id.recyclerViewMovies);
        recyclerViewMovies.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerViewMovies.setLayoutManager(layoutManager);

        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        recyclerViewMovies.addItemDecoration(itemDecoration);

        adapter = new MoviesAdapter(movies, recyclerViewMovies);
        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        page++;
                        sendRequest(page.toString());
                    }
                }, 1000);}
        });

        recyclerViewMovies.setAdapter(adapter);
        sendRequest(page.toString());
        swipeRefreshLayout.setRefreshing(true);
    }

    private void processJson() throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonResponse);
        JSONArray results = jsonObject.getJSONArray("results");

        for(int i=0; i<results.length(); i++){
            JSONObject movieObject = results.getJSONObject(i);
            movies.add(new Movie(movieObject.getInt("id"), movieObject.getString("title"), movieObject.getString("poster_path")));
        }
        swipeRefreshLayout.setRefreshing(false);
        adapter.setLoaded();
        adapter.notifyDataSetChanged();
    }

    private void sendRequest(String page){
        String url;
        if(mode == Mode.TOP_RATED){
            url = Constants.getTopRatedUrl(page);
        }else{
            url = Constants.getPopularUrl(page);
        }
        final Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.code()==200){
                    jsonResponse = response.body().string();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                        try {
                            processJson();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((MoviesAdapter) adapter).setOnItemClickListener( new MoviesAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(getApplicationContext(), MovieDetailsScreen.class);
                intent.putExtra("movie_id", movies.get(position).getId());
                startActivity(intent);
            }
        });
    }
}
