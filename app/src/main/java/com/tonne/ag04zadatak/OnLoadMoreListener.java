package com.tonne.ag04zadatak;

public interface OnLoadMoreListener {
    void onLoadMore();
}